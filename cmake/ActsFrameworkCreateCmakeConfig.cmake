# Create CMake relocatable config files

include(CMakePackageConfigHelpers)

# version is taken automatically from PROJECT_VERSION; no need to specify
write_basic_package_version_file(
  ${PROJECT_BINARY_DIR}/ActsFrameworkConfigVersion.cmake
  COMPATIBILITY SameMajorVersion)
configure_package_config_file(
  ${CMAKE_CURRENT_LIST_DIR}/ActsFrameworkConfig.cmake.in
  ${PROJECT_BINARY_DIR}/ActsFrameworkConfig.cmake
  INSTALL_DESTINATION ${CMAKE_INSTALL_DATAROOTDIR}/cmake/ActsFramework
  PATH_VARS CMAKE_INSTALL_BINDIR CMAKE_INSTALL_INCLUDEDIR CMAKE_INSTALL_LIBDIR)

# install core cmake configs
install(
  FILES
    ${PROJECT_BINARY_DIR}/ActsFrameworkConfigVersion.cmake
    ${PROJECT_BINARY_DIR}/ActsFrameworkConfig.cmake
  DESTINATION ${CMAKE_INSTALL_DATAROOTDIR}/cmake/ActsFramework)
# install target configs for all available components
foreach(_component ${_supported_components})
  install(
    EXPORT ActsFramework${_component}Targets
    DESTINATION ${CMAKE_INSTALL_DATAROOTDIR}/cmake/ActsFramework)
endforeach()

